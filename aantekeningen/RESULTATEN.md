Resultaten
==========
Tenzij anders genoteerd zijn gebruiken de algoritmes altijd unigrammen. In het
geval van FastText is hij 25 epochs getraind met een leergraad van 1.0.

## Mogelijkheden voor diepere analyse
1. Verfiëren dat teksten met ongenormaliseerde spelling (zoals oude PPCEME
   teksten) accuratesse, precisie, en recall negatief beïnvloeden.
2. Verwarringsmatrix bestuderen.

## Experimenten 2019-09-14
Alle algoritmes getraind op 1370 Archer documenten van de genres die voor het
classificeren van Emma relevant werden bevonden. Dat betekent dat er wel
geselecteerd was op genre, maar niet op jaar van publicatie. Van PPCEME waren
is op dezelfde manier geselecteerd op genre, maar ook op publicatiejaar, want
dit corpus was niet genormaliseerd en eigen normalisatiepogingen waren maar
half succesvol. Aangenomen werd dat de afwijkende spelling van de oude teksten
zoals uit de 15e eeuw te vertekenend zou werken.

De negen genres die Bragi in deze tests probeert te herkennen zijn:

1. Biografie (`biography`)
2. Catechismes (`catechism`)
3. Theater (`theater`)
4. Fictie (`fiction`)
5. Brieven (`letters`)
6. Wetten, rechtzaken en rechterlijke uitspraken (`legal`)
7. Sermoenen (`sermons`)
8. Wetenschappelijke documenten (`science`)
9. Reisverhalen (`travelogue`)

| Iteraties | Algoritme | Accuratesse |  Precisie | Recall    |
|----------:|:---------:|------------:|----------:|----------:|
|        20 | Bayes     |     89,9%   |   86,5%   |   84,5%   |
|        20 | SVM       |     90,5%   | **87,2%** |   79,5%   |
|        20 | FastText  |   **94,6%** |   84,3%   | **84,7%** |

| Iteraties | Algoritme | Accuratesse |  Precisie | Recall    |
|----------:|:---------:|------------:|----------:|----------:|
|       100 | Bayes     |    89,8%    |   86,1%   |   84,2%   |
|       100 | SVM       |    90,8%    | **90,5%** |   80,1%   |
|       100 | FastText  |  **93,9%**  |   84,0%   | **84,5%** |


## Experimenten 2019-09-14: Meer genres

In deze train- en testsessies is Coerp erbij gehaald. Van 147 teksten waren 78
betrokken in de training. Ook zijn in deze tests de 
genres uitgebreid met de volgende vier, voor een totaal van dertien. Van Coerp
zijn de teksten in de genres biografie, catechisme, sermoen, en gebeden 
opgenomen, teksten van het genre treatise_controv niet.

1. 'Advertenties' (`advertising`, let wel: zijn niet advertenties zoals wij die
   kennen)
2. Bijbel-gerelateerde documenten (`bible`)
3. Dagboeken, publieke of niet (`diary`)
4. Filosofie (`philosophy`)
4. News (`news`)

| Iteraties | Algoritme | Accuratesse |  Precisie | Recall    |
|----------:|:---------:|------------:|----------:|----------:|
|        10 | Bayes     |    82,6%    |   72,5%   |   67,6%   |
|        10 | SVM       |    87,5%    | **83,8%** |   74,8%   |
|        10 | FastText  |  **89,2%**  |   78,0%   | **77,1%** |

| Iteraties | Algoritme | Accuratesse |  Precisie | Recall    |
|----------:|:---------:|------------:|----------:|----------:|
|        20 | Bayes     |    82,8%    |   70,1%   |   66,5%   |
|        20 | SVM       |    88,3%    | **87,0%** |   77,0%   |
|        20 | FastText  |  **89,2%**  |   78,2%   | **77,4%** |