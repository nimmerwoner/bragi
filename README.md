# Bragi

Dit is de repository voor de genreclassifieerder voor Vroegmodern Engelse
teksten. Deze bevat een herimplementatie van Bragi 0.5/0.6 met een aangenamer
trainings- en toepassingsinterface.

Omdat de oude repository vervuild was geraakt met grote bestanden, is deze
gepurged. Ga naar [bragi/pre-1.0](http://nimmerfort.eu/bragi/middelen/pre-1.0/)
om de oude git repo met code of oude modellen te downloaden.

Bezoek ook [de website](http://nimmerfort.eu/bragi/).

## Een genreclassifieerder voor Vroegmodern Engelse teksten

[Schermafdruk van Bragi](https://bitbucket.org/nimmerwoner/bragi/raw/4bf4e55871b30f75756d70b35533eb5ce8cf52c2/middelen/schermafdruk.png)

* Kan teksten in een van de vroegmoderne genres categoriseren die voorkomen in
  bestaande corpora als ARCHER en PPCEME.
* Drie classificatiealgoritmes geïmplementeerd: Multinomiale Naïve Bayes, SVM
  en FastText.
* Classificaties exporteerbaar als csv.
* Getraind op ARCHER en PPCEME.

## Installeren

[B van Bragi](middelen/beeldmerk.svg) Bragi is beschikbaar via
Flatpak. Ook kan deze repository gekloond worden en `bragi_cli.py` of
`bragi_gtk.py` uitgevoerd worden.

1. `flatpak remote-add --if-not-exists --user flathub https://dl.flathub.org/repo/flathub.flatpakrepo`
2. `flatpak remote-add --if-not-exists --user --no-gpg-verify nimmerfort http://nimmerfort.eu/programmabank/`
3. `flatpak install nimmerfort eu.nimmerfort.Bragi`

## Literatuur

* KOWSARI (2019) *Text Classification Algorithms: A Survey*.
  [Artikel](https://arxiv.org/pdf/1904.08067)
* NLP-PROGRESS (2019) *Text Classification*.
  [Lijst](http://nlpprogress.com/english/text_classification.html) van status quo.
* BRIGHTMART (2019) *Text Classification*.
  [Allerhande](https://github.com/brightmart/text_classification) waaronder implementaties en modellen.
* COLLINS e.a. (2018) *Evolutionary Data Measures: Understanding the Difficulty of Text Classification Tasks*.
  [Artikel](https://www.aclweb.org/anthology/K18-1037).
* DU e.a. (2018) *Explicit Interaction Model towards Text Classification*
  [Breedpubliek](https://medium.com/@neurohive/exam-state-of-the-art-method-for-text-classification-75647834299) en [wetenschappelijk](https://arxiv.org/pdf/1811.09386) artikel.
* HOWARD en RUDER (2018) *Universal Language Model Fine-tuning for Text Classification*.
  [Wetenschappelijke](https://arxiv.org/pdf/1801.06146) en [breedpubliek](http://nlp.fast.ai/classification/2018/05/15/introducting-ulmfit.html) artikel.
* ZHANG e.a. (2016) *Character-level Convolutional Networks for Text Classification*.
  [Artikel](https://arxiv.org/pdf/1509.01626.pdf).
* LIU e.a. (2016) *Recurrent Neural Network for Text Classification with Multi-Task Learning*.
  [Artikel](https://arxiv.org/pdf/1605.05101.pdf).
* HUANG en WANG (2016) *Character-level Convolutional Network for Text Classification Applied to Chinese Corpus*.
  [Artikel](Character-level Convolutional Network for Text Classification Applied to Chinese Corpus).
* KIM (2014) *Convolutional Neural Networks for Sentence Classification*
  [Artikel](https://arxiv.org/pdf/1408.5882.pdf).
* CONEAU e.a. (2016) *Very Deep Convolutional Networks for Text Classification*
  [Artikel](https://arxiv.org/pdf/1606.01781) en [implementatie](https://github.com/zonetrooper32/VDCNN) en nog een [implementatie](https://github.com/WenchenLi/VDCNN).
* CAPITAL ONE TECH (2018) *The NLP Playbook, Part 1: Deep Dive into Text Classification*.
  [Artikel](https://medium.com/capital-one-tech/the-nlp-playbook-part-1-deep-dive-into-text-classification-61503144de78).
* DEVLIN e.a. (2018) *BERT: Pre-training of Deep Bidirectional Transformers for Language Understanding*.
  [Artikel](https://arxiv.org/pdf/1810.04805), eventueel met [CHANG e.a. aanpassingen](https://arxiv.org/pdf/1905.02331) voor een groot set aan labels.