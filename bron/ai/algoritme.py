import pickle
from typing import Optional, List, Tuple, Dict, Any
from nptyping import Array

import sklearn.metrics as sklm

from corpora import Document

class Algoritme:
    @staticmethod
    def precisie(
            feiten: List[str],
            voorspellingen: List[str]
    ) -> float:
        return sklm.precision_score(feiten, voorspellingen, average="macro")


    @staticmethod
    def recall(
            feiten: List[str],
            voorspellingen: List[str]
    ) -> float:
        return sklm.recall_score(feiten, voorspellingen, average="macro")


    @staticmethod
    def accuratesse(
            feiten: List[str],
            voorspellingen: List[str]
    ) -> float:
        return sklm.accuracy_score(feiten, voorspellingen)


    @staticmethod
    def confusiematrix(
            feiten: List[str],
            voorspellingen: List[str]
    ) -> float:
        genres = list(Document.genres.keys())
        return sklm.confusion_matrix(feiten, voorspellingen, genres)


    def naam() -> str:
        return self.__class__.__name__


    def train(self, documenten: List[Document]) -> "Algoritme":
        raise Exception("Not implemented")


    def test(
            self,
            documenten: List[Document],
            verwarring: bool = False
    ) -> Tuple[List[str], float, float, float]:
        feiten = [doc.genre() for doc in documenten]
        voorspellingen = self.voorspel(documenten)

        acc = Algoritme.accuratesse(feiten, voorspellingen)
        pre = Algoritme.precisie(feiten, voorspellingen)
        rec = Algoritme.recall(feiten, voorspellingen)
        conf = Algoritme.confusiematrix(feiten, voorspellingen) \
               if verwarring else None
        return voorspellingen, acc, pre, rec, conf


    def voorspel(self, documenten: List[Document]) -> List[str]:
        raise Exception("Not implemented")


    def test_en_print(self, documenten: List[Document] = []):
        voorspellingen, acc, pre, rec = self.test(documenten)
        self.print_verslag(len(documenten), acc, pre, rec)


    def print_verslag(self, N: int, a: float, p: float, r: float):
        print("N: " + str(N))
        print("A: {:0.3f};  P: {:0.3f};  R: {:0.3f}".format(a, p, r))
