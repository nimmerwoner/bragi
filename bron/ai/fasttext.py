import os
from typing import Optional, List, Tuple, Dict, Any
from nptyping import Array

from pprint import pprint

import fasttext
from fasttext.FastText import _FastText
from sklearn import metrics

from ai.algoritme import Algoritme
from corpora import Document


class FastText(Algoritme):
    datapath = "/tmp/bragi-fasttext.txt" #: str
    fasttext = None #: Optional[_FastText]
    train_args = {} #: Dict[str, Any]

    def __init__(self, getraind_model: Optional[str] = None, **train_args):
        self.train_args = train_args

        if getraind_model:
            self.fasttext = fasttext.load_model(getraind_model)


    def train(self, documenten: List[Document]) -> _FastText:
        self.export_docs(documenten, self.datapath)
        self.fasttext = fasttext.train_supervised(self.datapath,
                                                  **self.train_args)


    #def test(self, documenten: List[Document]) -> Tuple[List[str], float]:
        # FastTexts eigen testcode. Integreert niet netjes met die van Bragi
        #datapath = "/tmp/bragi-fasttext.txt"
        #self.export_docs(documenten, datapath)
        #results = self.fasttext.test(datapath)
        #self.print_results(*results)
        #return [], 0.0


    def voorspel(self, documenten: List[Document]) -> List[str]:
        if not self.fasttext:
            raise ValueError("Voorspelling gevraagd terwijl FastText nog niet getraind is")

        teksten = [" ".join(doc.woorden()) for doc in documenten]
        feiten  = [doc.genre() for doc in documenten]
        voorspellingen = self.fasttext.predict(teksten)

        # Verwijder het voorvoegsel '__label__' dat FastText voor labels zet
        voorspellingen = [vrsp[0][9:] for vrsp in voorspellingen[0]]
        return voorspellingen


    def bewaar(self, bewaarpad: str, retrain: bool = True):
        if not self.fasttext:
            raise ValueError("Opslaan verzocht terwijl FastText nog niet getraind is")

        if retrain:
            self.fasttext.quantize(input=self.datapath, retrain=retrain)
            self.fasttext.save_model(bewaarpad + ".ftz")
        else:
            self.fasttext.save_model(bewaarpad + ".bin")


    def test_en_print(self, documenten: List[Document]=[]):
        if documenten:
            self.export_docs(documenten, self.datapath)
        res = self.fasttext.test(self.datapath)
        N, P, R = res[0], res[1], res[2]
        self.print_verslag(N, 0.00, P, R)


    def export_docs(self, documenten: List[Document], datapath: str):
        # TODO error handling in case writing fails
        lines = ["__label__" + doc.genre() + " " + " ".join(doc.woorden())
                 for doc in documenten]
        with open(datapath, "w") as uit:
            uit.write("\n".join(lines))
