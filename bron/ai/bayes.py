import pickle
from typing import Optional, List, Tuple, Dict, Any
from nptyping import Array

from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
from sklearn.naive_bayes import MultinomialNB
from sklearn import metrics

from ai.algoritme import Algoritme
from corpora import Document


class Bayes(Algoritme):
    bayes = None #: Optional[MultinomialNB]
    vectorisant = None#: Optional[CountVectorizer]
    train_args = {} #: Dict[str, Any]
    vectoriseer_args = { #: Dict[str, Any]
        "strip_accents": "unicode",
        "ngram_range": (1, 1)
    }


    def __init__(self, getraind_model: Optional[str] = None, **train_args):
        if "fit_prior" not in train_args:
            train_args['fit_prior'] = False
        if "ngram_range" in train_args:
            num = train_args.pop("ngram_range")
            self.vectoriseer_args["ngram_range"] = (num, num)
        self.train_args = train_args

        if getraind_model:
            with open(getraind_model, 'rb') as fh:
                self.vectorisant, self.bayes = pickle.load(fh)


    def train(self, documenten: List[Document]) -> MultinomialNB:
        #print("Classificeren met een multinomiale naive bayes-methode")
        self.vectorisant, dtm = self.vind_documentkenmerken(documenten)
        genres = [doc.genre() for doc in documenten]


        self.bayes = MultinomialNB(**self.train_args)
        self.bayes.fit(dtm, genres)
        return self.bayes


    def voorspel(self, documenten: List[Document]) -> List[str]:
        if not self.bayes or not self.vectorisant:
            raise ValueError("Voorspelling gevraagd terwijl Bayes nog niet getraind is")

        teksten = [doc.tekst() for doc in documenten]
        dtm = self.vectorisant.transform(teksten)
        voorspellingen = self.bayes.predict(dtm)
        return voorspellingen


    def bewaar(self, bewaarpad: str, retrain: bool = True):
        if not self.bayes or not self.vectorisant:
            raise ValueError("Opslaan verzocht terwijl Bayes nog niet getraind is")

        # TODO Vervangen met betrouwbaardere serialiseringsmethode
        with open(bewaarpad + ".pkl", 'wb') as fh:
            pickle.dump((self.vectorisant, self.bayes), fh)


    def vind_documentkenmerken(self, documenten: List[Document]
    ) -> Tuple[CountVectorizer, Array]:
        """ In:  documenten = Een lijst van Document-objecten. Dit zijn de
                 documenten op basis waarvan Bragi moet trainen.
            Uit: Een twee-tupel van 1: de teller / het model;
                                    2: de document-term-matrix
        """
        #vectorisant = TfidfVectorizer( strip_accents="unicode"
        vectorisant = CountVectorizer(**self.vectoriseer_args)
        teksten = [doc.tekst() for doc in documenten]
        doc_term_matrix = vectorisant.fit_transform(teksten)
        return vectorisant, doc_term_matrix
