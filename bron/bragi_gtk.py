#!/usr/bin/python3

import os
import typing
import warnings
def warn(*args, **kwargs):
    pass
warnings.warn = warn # Godvergeten kut sklearn met z'n gedwongen warnings. Rot. Op.

import pandas as pd # import without using in this file to get around loading
                    # bug later: loading pandas after Gtk crashes pandas
import gi # type: ignore
gi.require_version('Gtk', '3.0')
from gi.repository import Gio, Gtk

from gtkui.hoofdvenster import Hoofdvenster

class Bragi(Gtk.Application):
    """ The top level class where the Boekerij application starts. Instantiates
        a Gtk Application which in turn sets up all the windows and actions.
    """

    hoofdvenster = None #: Optional[Hoofdvenster]

    def __init__(self, *args, **kwargs):
        """ Initiates the Application.
        """
        super().__init__(*args, application_id="eu.nimmerfort.Bragi",
                         flags=Gio.ApplicationFlags.FLAGS_NONE, **kwargs)

    def do_activate(self, *_):
        """ Sets up the windows and menu actions.
        """
        self.hoofdvenster = Hoofdvenster()
        self.add_window(self.hoofdvenster.venster)

        file_directory = os.path.dirname(os.path.abspath(__file__))
        hoofdmenu_ui = os.path.join(file_directory, "gtkui/middelen/hoofdmenu.ui")
        builder = Gtk.Builder.new_from_file(hoofdmenu_ui)
        menu = builder.get_object("hoofdmenu")
        self.set_app_menu(menu)

#        action = Gio.SimpleAction.new("overvenstertje", None)
#        action.connect("activate", self.on_about)
#        self.add_action(action)

        action = Gio.SimpleAction.new("afsluiting", None)
        action.connect("activate", self.on_quit)
        self.add_action(action)

        self.hoofdvenster.venster.present()


#    def on_about(self, *_):
#        """ Callback for showing the about dialog.
#        """
#        definition = "about_window.ui"
#        builder = Gtk.Builder.new_from_file(definition)
#        about = builder.get_object("overvenstertje")
#        about.set_transient_for(self.hoofdvenster.venster)
#        about.set_modal(True)
#        about.present()


    def on_quit(self, *_):
        """ Callback for the Quit menu item that quits this application.
        """
        self.quit()



if __name__ == "__main__":
    Bragi().run()
