#!/usr/bin/python3

import argparse
import warnings
def warn(*args, **kwargs):
    pass
warnings.warn = warn # Godvergeten kut sklearn met z'n gedwongen warnings. Rot. Op.

from cli.train import train_dist
from cli.test import test_met_willekeur_herhaald

if __name__ == "__main__":
    #print("\nBragi (2017-2019), Arthur Nieuwland")
    parser = argparse.ArgumentParser(description='Train en test Bragi modellen')
    parser.add_argument('opdracht', type=str, choices=['test', 'dist'],
        help="De opdracht die uitgevoerd moet worden. Met `test` traint en test Bragi de ondersteunde algoritmen 100 keer volgens het principe van herhaalde validatie met willekeurige subsampling. Met `dist` worden die algoritmen getraind en opgeslagen geschikt voor distributie.")
    parser.add_argument('--herhalingen', type=int, default=20, help="Hoevaak een algoritme getraind en getest moet worden met willekeurig samengestelde train en test sets. Alleen relevant voor de `test` opdracht. Standaard is het 20.")
    parser.add_argument('--verwarring', default=False, action='store_true', help="Of een verwarringsmatrix berekend moet worden. Deze wordt weggeschreven naar /home/anieuwland/confusie")

    args = parser.parse_args()

    if args.opdracht == "test":
        test_met_willekeur_herhaald(args.herhalingen, args.verwarring)
    elif args.opdracht == "dist":
        train_dist()
