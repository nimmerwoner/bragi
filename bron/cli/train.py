#!/usr/bin/python3

import os
from typing import List
from datetime import date

from ai import Bayes, SVM, FastText
from corpora import lees_bragi, Document


def train_dist():
    algoritmen = definieer_algoritmen()
    documenten = lees_bestanden()
    uitpad = "/home/anieuwland"
    print()

    for naam, algoritme in algoritmen:
        print("Trainen van " + naam)
        _train_dist(documenten, algoritme, uitpad)
        print("Resultaten op geziene data:")
        algoritme.test_en_print(documenten)
        print()


def definieer_algoritmen():
    algoritmen = [
        ("Bayes-unigrams", Bayes()),
        ("SVM-unigrams", SVM()),
        ("FastText-25epochs-unigrams-1.0lr", FastText(epoch=25, verbose=0,
                                                      wordNgrams=1, lr=1.0)),
    ]
    return algoritmen


def _train_dist(documenten: List[Document], algoritme: object, uitpad: str):
    algoritme.train(documenten)
    bestandsnaam = formuleer_bestandsnaam(algoritme)
    bestandspad = os.path.join("/home/anieuwland/", bestandsnaam)
    print("Opslaan als", bestandspad, end="... ")
    algoritme.bewaar(bestandspad)
    print("Voltooid")


def lees_bestanden():
    archer = "/home/anieuwland/Ontwikkeling/bragi/corpora/herschikt/archer"
    ppceme = "/home/anieuwland/Ontwikkeling/bragi/corpora/herschikt/ppceme"
    archer_zeef = lambda doc: doc.genre() != Document.NEGEER

    documenten = []
    documenten.extend(lees_bragi(archer, zeef=archer_zeef))
    documenten.extend(lees_bragi(ppceme, zeef=lambda doc: doc.relevant()))
    return documenten


def formuleer_bestandsnaam(class_instance: object):
    bestandsnaam = class_instance.__class__.__name__
    bestandsnaam += "_" + str(date.today())
    return bestandsnaam
