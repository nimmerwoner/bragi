#!/usr/bin/python3
from typing import List, Tuple
from datetime import date
#from queue import Queue
#from collections import deque
#from random import shuffle
#from operator import add
#from threading import Thread

import numpy as np
from sklearn.model_selection import train_test_split

from ai import Bayes, SVM, FastText
from corpora import lees_bragi, lees_coerp, Document


def test_met_willekeur_herhaald(
        herhalingen: int = 100,
        verwarring: bool = False
    ):
    gemiddeldes = []
    documenten = lees_bestanden()
    algoritmen = definieer_algoritmen()
    for naam, algoritme in algoritmen:
        print("Testen van " + naam + "...")
        acc, pre, rec, conf = test_met_willekeur( # Gemiddelde acc, pre, rec
            documenten,
            algoritme,
            herhalingen=herhalingen,
            verwarring=verwarring
        )
        gemiddeldes.append((naam, acc, pre, rec, conf))

    for naam, acc, pre, rec, conf in gemiddeldes:
        bericht = "Gemiddelde ({}) over {} keer is A: {:0.3f};  P: {:0.3f};  R: {:0.3f}"
        bericht = bericht.format(naam, herhalingen, acc, pre, rec)
        print(bericht)
        if verwarring:
            print("Verwarringsmatrix:")
            #np.savetxt("/home/anieuwland/Confusiematrix-" + naam, conf)
            print(conf)
            print()


def test_met_willekeur(documenten: List[Document], algoritme: object,
                       herhalingen: int = 100, train_deel: float = 0.7,
                       test_deel: float = 0.3, opslaan: bool = False,
                       verwarring: bool = False
    ) -> Tuple[float, float, float]:
    # Berekeningen om resultaten mooi weer te geven. Niet essentieel voor testen
    max_lengte = str(len(str(herhalingen)))
    uit = "{: "+max_lengte+"}/{} - A: {:0.3f};  P: {:0.3f};  R: {:0.3f}"

    # De test zelf
    max_lengte = 0
    uitslag_acc = 0.0
    uitslag_pre = 0.0
    uitslag_rec = 0.0
    confusies = None
    for iteratie in range(herhalingen):
        train_docs, test_docs = train_test_split(documenten,
                                                 train_size=train_deel,
                                                 test_size=test_deel)
        algoritme.train(train_docs)
        _, acc, pre, rec, conf = algoritme.test(test_docs, verwarring)
        uitslag_acc += acc
        uitslag_pre += pre
        uitslag_rec += rec
        if verwarring:
            confusies = conf if confusies is None \
                             else np.add(confusies, conf)

        bericht = uit.format(iteratie+1, herhalingen, acc, pre, rec)
        max_lengte = len(bericht) if len(bericht) > max_lengte else max_lengte
        print(bericht, end="\r")
    gemiddelde_acc = uitslag_acc/herhalingen
    gemiddelde_pre = uitslag_pre/herhalingen
    gemiddelde_rec = uitslag_rec/herhalingen
    confusies = np.divide(confusies, herhalingen).astype('uint8') \
                if verwarring else None

    # Regel opschonen voor volgende print
    witregel = [" "] * max_lengte
    witregel = "".join(witregel)
    print(witregel, end="\r")
    return gemiddelde_acc, gemiddelde_pre, gemiddelde_rec, confusies


def lees_bestanden():
    archer = "/home/anieuwland/Ontwikkeling/bragi/corpora/bragi-behandeld/archer"
    ppceme = "/home/anieuwland/Ontwikkeling/bragi/corpora/bragi-behandeld/ppceme"
    coerp  = "/home/anieuwland/Ontwikkeling/bragi/corpora/origineel/coerp"
    archer_zeef = lambda doc: doc.genre() != Document.NEGEER

    documenten = []
    documenten.extend(lees_bragi(archer, zeef=archer_zeef))
    documenten.extend(lees_bragi(ppceme, zeef=lambda doc: doc.relevant()))
    documenten.extend(lees_coerp(coerp, zeef=lambda doc: doc.relevant()))
    return documenten


def definieer_algoritmen():
    algoritmen = [
    #    ("FastText-5epochs-unigrams-1.0lr", FastText(epoch=5, verbose=0,
    #                                                 wordNgrams=1, lr=1.0)),
    #    ("FastText-100epochs-unigrams-1.0lr", FastText(epoch=100, verbose=0,
    #                                                   wordNgrams=1, lr=1.0)),
    #    ("FastText-5epochs-bigrams-1.0lr", FastText(epoch=5, verbose=0,
    #                                                wordNgrams=2, lr=1.0)),
    #    ("FastText-25epochs-bigrams-1.0lr", FastText(epoch=25, verbose=0,
    #                                                 wordNgrams=2, lr=1.0)),
    #    ("FastText-100epochs-bigrams-1.0lr", FastText(epoch=100, verbose=0,
    #                                                  wordNgrams=2, lr=1.0)),
        ("Bayes", Bayes()),
        ("SVM", SVM()),
        ("FastText-25epochs-unigrams-1.0lr", FastText(epoch=25, verbose=0,
                                                      wordNgrams=1, lr=1.0)),
    #    ("FastText-1epochs-unigrams-1.0lr", FastText(epoch=1, verbose=0,
    #                                                  wordNgrams=1, lr=1.0)),
    ]
    return algoritmen


#def test_parallel(herhalingen: int = 100, train_deel: float = 0.7,
#                  test_deel: float = 0.3) -> Tuple[float, float, float]:
#    # Maak een ongesorteerde wachtrij van taken om uit te voeren
#    algoritmen = []
#    [algoritmen.extend([alg] * herhalingen) for alg in definieer_algoritmen()]
#    shuffle(algoritmen)
#    wachtrij = Queue(len(algoritmen))
#    [wachtrij.put(alg) for alg in algoritmen]

#    # Bestanden inlezen en resultaatverzameling definiëren
#    takental = len(algoritmen)
#    documenten = lees_bestanden()
#    resultaten = deque(maxlen=takental)

#    # Werk tegelijkertijd de traintest-taken af in de wachtrij met en bewaar
#    # resultaten
#    def werk(takental):
#        uit = "{} - {: "+str(takental)+"}/{} - A: {:0.3f};  P: {:0.3f};  R: {:0.3f}"
#        max_lengte = 0
#        while not wachtrij.empty():
#            naam, algoritme = wachtrij.get()
#            train_docs, test_docs = train_test_split(documenten,
#                                                     train_size=train_deel,
#                                                     test_size=test_deel)
#            algoritme.train(train_docs)
#            _, acc, pre, rec = algoritme.test(test_docs)
#            resultaten.append((naam, acc, pre, rec))
#            wachtrij.task_done()

#            bericht = uit.format(naam, len(resultaten)+1, takental, acc, pre, rec)
#            max_lengte = len(bericht) if len(bericht) > max_lengte else max_lengte
#            print(bericht, end="\r")
#    werkers = []
#    for _ in range(4):
#        werker = Thread(target=werk, args=(takental,))
#        werker.setDaemon(True)
#        werker.start()
#        werkers.append(werker)
#    [w.join() for w in werkers]

#    # Bereken en print resultaten
#    totalen = {}
#    totalen = {r[0]: map(add, totalen.get(r[0], (0, 0, 0)), r[1:])
#               for r in resultaten} # sommeer alle resultaten
#    for naam, (acc, pre, rec) in totalen.items():
#        bericht = "Gemiddelde ({}) over {} keer is A: {:0.3f};  P: {:0.3f};  "
#        bericht += "R: {:0.3f}"
#        gemiddelde_acc = acc / herhalingen # deel voor gemiddelde
#        gemiddelde_pre = pre / herhalingen
#        gemiddelde_rec = rec / herhalingen
#        bericht = bericht.format(naam, herhalingen, gemiddelde_acc,
#                                 gemiddelde_pre, gemiddelde_rec)
#        print(bericht)
