from os import listdir
from os.path import isfile, join, isdir, basename, dirname, exists
from re import sub, split, search, compile, MULTILINE
from lxml import etree

from corpora.document import Document

# Helsinki wordt niet gebruikt om op te trainen want de proza erin overlapt met
# die in andere corpora, en het bevatte geen verzen in de periode die Bragi
# interesseert. Het onderstaande is dan ook een restant
class HelsinkiDocument(Document):
    naam = 'Helsinki'
    genres = {}

    def __init__(self, bestand):
        self.genres = PpcemeDocument.genres
        self.bestand = bestand
        super().__init__(bestand)

    def jaar(self):
        return self.metainfo('O')

    def genre(self):
        tekst = self.genre_tekst().split('+')[0]
        return genres[tekst]

    def genre_tekst(self):
        genre_tekst = self.metainfo('T').replace('/', ' ').lower()
        vorm_tekst = self.vorm()
        geheel = genre_tekst + "+" + vorm_tekst
        return geheel

    def vorm(self):
        return self.metainfo('V').lower()

    def metainfo(self, code):
        regels = super().tekst().split("\n")
        metasleutel = "<" + code + " "
        genre = None
        for regel in regels:
            if regel[:3] == metasleutel:
                metawaarde = regel.strip()[3:-1]
        return metawaarde

    def tekst(self):
        tekst = super().tekst()
        tekst = tekst.replace('(^', '')  # ander lettertype
        tekst = tekst.replace('^)', '')
        tekst = tekst.replace('#', '')   # iets
        tekst = tekst.replace('(\\', '') # andere taal
        tekst = tekst.replace('\\)', '')
        tekst = tekst.replace('(}', '')  # runen
        tekst = tekst.replace('})', '')
        tekst = tekst.replace('[}', '')  # kop
        tekst = tekst.replace('}]', '')

        tekst = self.verwijder_bereik('[{', '{]', tekst) # emendatie
        tekst = self.verwijder_bereik('[\\', '\\]', tekst) # commentaar
        tekst = self.verwijder_bereik('[^', '^]', tekst)

        return tekst

    def verwijder_bereik(self, begin, eind, tekst):
        # NOOT: niet zeker dat onderstaande code juist is - een test gaf
        # niet het gewenste resultaat. Dit corpus bleek verder niet nuttig, dus
        # is niet verder ontwikkeld
        idx_begin = tekst.find(begin)
        if idx_begin != -1:
            idx_eind = tekst.find(eind, idx_begin)
            if idx_eind != -1:
                tekst = tekst[ : idx_begin] + tekst[idx_eind + len(eind) : ]
            else:
                print("Fout! Wel een begin-{}, maar geen eind-{}".format(begin, eind))
        return tekst.strip()

    def interessant(self):
        return True
        #naam = self.bestandsnaam()
        #return naam[1:2].lower() == 'e' # E voor Early Modern English

    @staticmethod
    def lees_in(pad):
        negeerlijst = ['One_file', 'HCextrafiles_info.xls']
        docs = [HelsinkiDocument(bestand.bestand) for bestand
                in Document.lees_in(pad, HelsinkiDocument,
                                    negeerlijst=negeerlijst)]
        print("{} gevonden".format(len(docs)))
        return docs
