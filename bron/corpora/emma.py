from os import listdir
from os.path import isfile, join, isdir, basename, dirname, exists
from re import sub, split, search, compile, MULTILINE
from lxml import etree

from corpora.document import Document


# Klasse om documenten uit emma-rauw te begrijpen
class EmmaDocument(Document):
    naam = "EMMA"
    genres = { "science": Document.SCIENCE
             , "science_chemistry": Document.SCIENCE
             , "science_geography": Document.SCIENCE
             , "science_mathematics": Document.SCIENCE
             , "science_physics": Document.SCIENCE
             , "science_medicine": Document.SCIENCE
             , "legal": Document.LEGAL
             , "legal?": Document.LEGAL
             , "letters": Document.LETTERS
             , "letters_monitory": Document.LETTERS
             , "letters_pastoral": Document.LETTERS
             , "sermons": Document.SERMONS
             , "sermons_election": Document.SERMONS
             , "sermons_execution": Document.SERMONS
             , "sermons_fast-day": Document.SERMONS
             , "sermons_funeral": Document.SERMONS
             , "fiction": Document.FICTION
             , "drama": Document.THEATER
             , "drama_comedy": Document.THEATER
             , "drama_farce": Document.THEATER
             , "drama_masque": Document.THEATER
             , "drama_opera": Document.THEATER
             , "drama_prologue/epilogue": Document.THEATER
             , "drama_tragedy": Document.THEATER
             , "drama_tragicomedy": Document.THEATER
             , "biography/memoirs": Document.BIOGR
             , "v_advertisement": Document.LETTERS
             , "hymns/psalms": Document.SERMONS
             , "catechism": Document.CATECHISM

             , "prose": Document.UNKNOWN
             , "dialogue/conference": Document.UNKNOWN
             , "speech": Document.UNKNOWN
             , "v_fable": Document.UNKNOWN
             , "v_parable": Document.UNKNOWN
             , "v_prayer": Document.UNKNOWN
             , "v_testimony": Document.UNKNOWN
             , "miscellany": Document.UNKNOWN
             , "poetry": Document.UNKNOWN
             , "poetry_burlesque": Document.UNKNOWN
             , "poetry_elegy": Document.UNKNOWN
             , "poetry_epic": Document.UNKNOWN
             , "poetry_epigram": Document.UNKNOWN
             , "poetry_heroic": Document.UNKNOWN
             , "poetry_miscellany": Document.UNKNOWN
             , "poetry_occasional": Document.UNKNOWN
             , "songs": Document.UNKNOWN
             , "satire": Document.UNKNOWN
             , "undetermined": Document.UNKNOWN
             , "na": Document.UNKNOWN
             , "letters+legal": Document.UNKNOWN
             , "prose+drama": Document.UNKNOWN
             , "prose+sermons": Document.UNKNOWN
             , "science+letters": Document.UNKNOWN
             , "sermons_election": Document.UNKNOWN
             , "poetry_miscellany+miscellany": Document.UNKNOWN
             , "prose+legal": Document.UNKNOWN
             , "prose+catechism": Document.UNKNOWN
             , "prose?": Document.UNKNOWN
             , 'sermons_execution+dialogue/conference': Document.UNKNOWN
             , 'prose+v_catalogue': Document.UNKNOWN
             , 'prose+letters': Document.UNKNOWN
             , 'prose+biography/memoirs': Document.UNKNOWN
             , 'prose+satire': Document.UNKNOWN
             , 'poetry_miscellany+songs': Document.UNKNOWN
             , 'v_journal+letters': Document.UNKNOWN
             }
    xml = None

    # Het emma-genre zoals door Bragi geinterpreteerd en afgebeeld op zijn
    # taxonomie
    def genre(self):
        if self.xml == None:
            xml = etree.parse(self.bestand)
        genre = xml.find(".//genre")
        if isinstance(genre, list):
            genre = genre[0]

        if genre == "legal?":
            print("Gevonden legal?: {}".format(self.bestandsnaam()))

        genre = genre.text.strip().lower()
        if not genre in self.genres:
            return Document.UNKNOWN
        else:
            return self.genres[genre]

    # Het zuivere emma-genre zoals het door Lynn is getagd zonder dat Bragi het
    # interpreteert
    def genre_tekst(self):
        if self.xml == None:
            xml = etree.parse(self.bestand)
        return xml.find(".//genre").text

    def tekst(self):
        if self.xml == None:
            xml = etree.parse(self.bestand)

        bodys = xml.xpath(".//doc")
        if len(bodys) == 1:
            return ' '.join(bodys[0].itertext()).strip()
        else:
            str = "FOUT: bestand had niet precies 1 body: {0}"
            print(str.format(self.bestand))
            exit(1)

    @staticmethod
    def lees_in(pad, stil=False):
        docs = [EmmaDocument(bestand.bestand) for bestand in
                Document.lees_in(pad, EmmaDocument, stil=stil)]
        docs = [doc for doc in docs if doc.genre() != Document.NEGEER]

        if not stil:
            print("{} gevonden".format(len(docs)))

        return docs
