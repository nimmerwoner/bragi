from os import listdir
from os.path import isfile, join, isdir, basename, dirname, exists
from re import sub, split, search, compile, MULTILINE
from lxml import etree

from corpora.document import Document


# Klasse om penn-documenten zoals in het oorspronkelijke, onbewerkte
# PPCEME-corpus voorkomen te begrijpen
class PpcemeDocument(Document):
    naam = "PPCEME"
    # Verbanden tussen PENNs genre-afkortingen en bragi's categorieen
    genres = { "biogr other": Document.BIOGR
             , "drama comedy": Document.THEATER
             , "fiction": Document.FICTION
             , "let non-priv": Document.LETTERS
             , "let priv": Document.LETTERS
             , "proc trial": Document.LEGAL
             , "science medicine": Document.SCIENCE
             , "science other": Document.SCIENCE
             , "sermon": Document.SERMONS
             , "travelogue": Document.TRAVEL

             # Te negeren
             , "bible": Document.BIBLE,        "biogr auto": Document.BIOGR
             , "diary priv": Document.DIARY,   "educ treat": Document.NEGEER
             , "handb other": Document.NEGEER, "history":    Document.HISTORY
             , "law": Document.LEGAL,          "philosophy": Document.PHILOSOPHY
             }

    tekstregels = None

    def jaar(self):
        if self.tekstregels == None:
            self.tekstregels = self.lees_infotekst()
        (gevonden, jaar) = self.vind_voor_sleutel(self.tekstregels, "Date")

        if not gevonden:
            bericht = "Jaar kon niet gevonden worden in {}"
            bericht = bericht.format(self.bestandsnaam())
            raise SyntaxError(bericht)

        # Schoon html-resten op (is nl omgeven met <td ..> .. </td>)
        jaar = search(r'([0-9]{4})', jaar).group(0)
        return jaar

    def genre(self):
        genre_afk = self.genre_tekst()
        return self.genres[genre_afk]

    def genre_tekst(self):
        if self.tekstregels == None:
            self.tekstregels = self.lees_infotekst()
        (gevonden, genre) = self.vind_voor_sleutel(self.tekstregels, "Genre")

        if not gevonden:
            bericht = "Genre kon niet gevonden worden in {}"
            bericht = bericht.format(self.bestandsnaam())
            raise SyntaxError(bericht)

        # Schoon html-resten op (is nl omgeven met <td ..> .. </td>)
        genre = search(r'>[a-zA-Z0-9 -]+</', genre).group(0)[1:-2].strip()
        return genre

    def tekst(self):
        inhoud = super().tekst()

        # Verwijder alles tussen < en >; en { en }
        inhoud = sub(r'<.+?>', "", inhoud)
        inhoud = sub('\{.+\}', "", inhoud)
        inhoud = inhoud.replace("$", "")


        # Verwijder alles voorafgegaan door de bestandsnaam en gevolgd door
        # een nieuwe regel. De bestandsnaam is in onderkast, maar in de tekst
        # in bovenkast en zonder extensie
        loop = True
        naam = self.bestandsnaam().upper()[:-4]
        while (loop):
            begin = inhoud.find(naam)
            einde = inhoud.find("\n", begin)
            deel  = inhoud[begin:einde]
            if begin >= 0: # als find een voorkomen gevonden heeft, verwijder die
                inhoud = inhoud.replace(deel, "", 1)
            else:
                loop = False

        return inhoud.strip()

    def interessant(self):
        try:
            return self.genre() != Document.NEGEER
        except:
            return False

    @staticmethod
    def lees_in(pad):
        negeer = [ 'harleyedw-e2-p1.txt', 'stat-1690-e3-h.txt',
                   'stat-1690-e3-p1.txt', 'stat-1670-e3-p2.txt',
                   'stat-1620-e2-p2.txt', 'stat-1600-e2-h.txt',
                   'stat-1640-e2-p2.txt', 'stat-1530-e1-p1.txt',
                   'stat-1550-e1-p2.txt', 'stat-1540-e1-p1.txt',
                   'stat-1500-e1-h.txt',  'stat-1540-e1-h.txt',
                   'stat-1590-e2-h.txt',  'stat-1600-e2-p2.txt',
                   'stat-1580-e2-h.txt',  'stat-1570-e2-p1.txt',
                   'stat-1560-e1-p2.txt', 'harleyedw-e2-p2.txt',
                   'stat-1510-e1-p2.txt', 'stat-1580-e2-p2.txt',
                   'stat-1660-e3-p2.txt', 'stat-1570-e2-p2.txt' ]
        docs = [PpcemeDocument(bestand.bestand) for bestand
                in Document.lees_in(pad, PpcemeDocument, negeer)]
        print("{} gevonden".format(len(docs)))
        return docs

    def lees_infotekst(self):
        info = self.vind_infobestand()

        inhoud = ""
        with open(info) as f:
            inhoud = f.read()
        regels = inhoud.splitlines() # Vind regel met genre
        return regels

    def vind_voor_sleutel(self, inforegels, sleutel):
        volgende = False             # Is de volgende regel die met het genre
        waarde = ""
        for regel in inforegels:
            if volgende:
               waarde = regel
               break
            if sleutel in regel:
                volgende = True

        if not volgende:
            print("Waarde-regel niet gevonden")
        return (volgende, waarde.lower())

    def vind_infobestand(self):
        # Het bestand zit in <pad>/txt/bestand.txt. Het html-bestand met de metadata
        # bevindt zich relatief aan dit pad in <pad>/../info/bestand.html.
        # De namen van de txt en info bestanden zijn niet volledig gelijk: het info
        # bestand heeft geen -h, -p1, of p2 achtervoegsel (de txt wel).
        naam = self.bestandsnaam()           # vind basisnaam
        stam = naam.replace("-h", "")        # verwijder addendum annotatie
        stam = stam.replace("-p1", "")       #     nu hebben we dezelfde naam, als
        stam = stam.replace("-p2", "")       #     het html-metadatabestand
        stam = stam[:-4]                     # verwijder onnodige .txt extensie
        stam = stam.replace("-a-", "-")
        stam = stam.replace("-b-", "-")
        stam = stam.replace("-c-", "-")
        stam = sub(r"-[0-9]{4}-", "-", stam) # verwijder jaartal
        stam = stam[::-1].replace("-", ".", 1)[::-1] # info-bestn gebruiken . ipv -

        # Morelet's info-bestand volgt de conventie niet, definiëer handmatig
        if stam[0:7] == "morelet":
            stam = "morelet.e1"

        # Soms staat het info bestaat samen met een ander info bestand
        # Dan hebben ze .e12 ipv .e1 of .e2
        info = dirname(self.bestand) + "/../../info/" + stam + ".html"

        if not exists(info):
            stamdelen = [ stam.split(".")[0]
                        , "e12"
                        , "html" ]
            info = dirname(self.bestand) + "/../../info/" + '.'.join(stamdelen)

        if not exists(info):
            bericht = "Geen info bestand {} voor {}"
            bericht = bericht.format(info, naam)
            raise FileNotFoundError(bericht)
        return info
