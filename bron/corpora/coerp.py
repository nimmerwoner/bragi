from os import listdir
from os.path import isfile, join, isdir, basename, dirname, exists
from re import sub, split, search, compile, MULTILINE
from lxml import etree

from corpora.document import Document


# Klasse om Coerp-documenten zoals in het oorspronkelijke, onbewerkte
# Coerp-corpus voorkomen te begrijpen
class CoerpDocument(Document):
    naam = "Coerp"
    genres = { "bio": Document.BIOGR
             , "catechism": Document.CATECHISM
             , "sermon": Document.SERMONS
             , "prayers": Document.PRAYERS
             , "treatise_controv": Document.NEGEER
             , "treatisecontrov": Document.NEGEER # HACK. Vervanging voor
             # genre erboven. Dit had BragiDocument nodig om te kunnen splitsen
             # op '_'
             }

    genret  = None   # Genretekst
    jaartal = None
    auteur  = None
    xml     = None

    luie_tekst_coerp = None #: Optional[str]

    def __init__(self, bestand):
        super().__init__(bestand)

        # Opsplitsen met een regex. Dat is nodig door de '_' in t_c, anders had
        # ik gewoon kunnen splitsen op '_'
        rx = r'_([0-9]{4})_' # splits op het jaartal, maar onthoud het
        bestandsnaam = self.bestandsnaam()[:-7] # haal _R3.xml eraf

        delen = split(rx, bestandsnaam)
        self.genret  = delen[0]
        self.jaartal = delen[1]
        self.auteur  = delen[2]

    def jaar(self):
        return self.jaartal

    def genre(self):
        return self.genres[self.genret]

    def genre_tekst(self):
        return self.genret

    def tekst(self):
        if not self.luie_tekst_coerp:
            if not self.xml:
                xml = etree.parse(self.bestand)

            bodys = xml.xpath(".//text")
            if len(bodys) == 1:
                # COERP teksten bestaan uit verschillende samples en daartoe is
                # tekst opgenomen die niet in het oorspronkelijke document stond
                for sample_aankondiging in bodys[0].xpath("//sampleN"):
                    sample_aankondiging.getparent().remove(sample_aankondiging)

                # Concateneer tekst
                tekst = ' '.join(bodys[0].itertext()).strip()
                self.luie_tekst_coerp = tekst
            else:
                str = "FOUT: bestand had niet precies 1 text-element: {}"
                print(str.format(self.bestand))
                exit(1)
        return self.luie_tekst_coerp

    def relevant(self):
        jaar = 0
        try:
            jaar = int(self.jaar())
        except:
            pass

        return self.genre() != Document.NEGEER and jaar > 1569

    @staticmethod
    def lees_in(pad, zeef=lambda x: True):
        docs = [CoerpDocument(bestand.bestand) for bestand in
                Document.lees_in(pad, CoerpDocument, ["list.txt"], zeef)]
        print("{} gevonden".format(len(docs)))
        return docs
