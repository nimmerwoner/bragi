from corpora.document import Document
from corpora.ppceme import PpcemeDocument
from corpora.ppcmbe import PpcmbeDocument
from corpora.archer import ArcherDocument
from corpora.clmet import ClmetDocument
from corpora.coerp import CoerpDocument

# Klasse om een Bragi-document te begrijpen. Een Bragi-document is een verwerkte
# variant van een bestand uit een corpus, vaak voor-geinterpreteerd met een van
# onderstaande klassen. Zo is belangrijke metadata al in de bestandsnaam geplakt
# te weten het jaartal en genre. Om niet verloren te laten gaan uit welk corpus
# een bestand komt, staat ook dat in de bestandsnaam. Om unieke namen mogelijk
# te maken sluit een willekeurige, betekenisloze combinatie van een cijfer en
# letter de bestandsnaam af. Deze dragen dus absoluut geen informatie.
# Een voorbeeld is ppceme_1608_let priv_x2.txt of
#                  coerp_1546_sermon_i4.txt
#
# Bragi bestanden zijn belangrijk / nuttig, omdat:
#     1. ze alle troep/ruis al uit een tekst gehaald hebben (vooral vreemde xml)
#     2. ze genormaliseerd zijn met Vard (handmatig gedaan buiten Bragi om)
#     3. metadata in hun bestandsnaam vervat hebben voor gemakkelijk gebruik
#        itt extern opgeslagen (looking at you PPCEME, wtf serieus)
#
# Waarschuwing: "Bragi-corpora", om ze zo te noemen, zijn nog _niet_ gefilterd
#               op jaar. Dat wil zeggen dat ook oninteressante jaren er in
#               zitten en nog door onderstaande klasse de interessante eruit
#               gevist moeten worden.
class BragiDocument(Document):
    genres = None
    naam = "Bragi"

    def __init__(self, bestand):
        self.bestand = bestand
        self.genres = dict(CoerpDocument.genres, **PpcemeDocument.genres)
        self.genres = dict(self.genres, **ArcherDocument.genres)
        self.genres = dict(self.genres, **ClmetDocument.genres)
        self.genres = dict(self.genres, **PpcmbeDocument.genres)

    def jaar(self):
        delen = self.bestandsnaam().split('_')
        return delen[1] # deel[0] is het corpus, 1 het jaar, 2 het genre

    def genre(self):
        return self.genres[self.genre_tekst()]

    def genre_tekst(self):
        delen = self.bestandsnaam().split('_')
        return delen[2]

    def corpus(self):
        delen = self.bestandsnaam().split('_')
        return delen[0]

    @staticmethod
    def lees_in(pad, zeef=lambda x: True):
        docs = [BragiDocument(bestand.bestand) for bestand
                in Document.lees_in(pad, BragiDocument, zeef=zeef)]
        #print("(oorspr. {})".format(self.corpus()), end="…  ")

        print("{} gevonden".format(len(docs)))
        return docs
