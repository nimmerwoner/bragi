from os import listdir
from os.path import isfile, join, isdir, basename, dirname, exists

import string

class Document:
    bestand = None

    # De categorieen die bragi kent
    ADVERTISING = "advertising"
    BIBLE = "bible"
    BIOGR   = "biography"
    CATECHISM = "catechism"
    DIARY = "diary"
    THEATER = "theater"
    FICTION = "fiction"
    HISTORY = "history"
    LETTERS = "letters"
    LEGAL   = "legal"
    NEWS    = "news"
    PHILOSOPHY = "philosophy"
    PRAYERS = "prayers"
    SERMONS = "sermons"
    SCIENCE = "science"
    TRAVEL  = "travelogue"
    UNKNOWN = "unknown"

    genres = {
        "advertising": ADVERTISING,
        "bible": BIBLE,
        "biography": BIOGR,
        "catechism": CATECHISM,
        "diary": DIARY,
        "theater": THEATER,
        "fiction": FICTION,
        "history": HISTORY,
        "letters": LETTERS,
        "legal": LEGAL,
        "news": NEWS,
        "philosophy": PHILOSOPHY,
        "prayers": PRAYERS,
        "sermons": SERMONS,
        "science": SCIENCE,
        "travelogue": TRAVEL,
        "unknown": UNKNOWN,
    }

    # Rest-categorieën waar we niets mee doen, maar voor een consistente werking
    # van de trainer vangen we ze wel af
    NEGEER = "negeer"

    # Categorieën die bragi kende, maar onnuttig zijn verklaard
    # Bewaard zodat er makkelijk naar terug gekeerd kan worden
    #BIOGR_AUTO = "biography-auto"
    #EDUCATIONAL = "educational"
    #HANDBOOK = "handbook" # "handbook other" in penn, maar geen andere handbook
    #JOURNAL = "journals"
    #LAW = "law"
    #LEGAL = "legal" # is wellicht hetzelfde als LAW of PROCEEDINGS_TRIALS
    #LETTERS_NP = "letters-nonprivate"
    #LETTERS_PR = "letters-private"
    #PHILOSOPHY = "philosophy"
    #SCIENCE_MED = "science-medicine"
    #SCIENCE_OTHER = "science-other"
    #RELIGIOUS = "religious"
    #PRAYERS = "prayers"

    luie_tekst = None #: Optional[str]


    def __init__(self, bestand):
        self.bestand = bestand

    # Geeft alleen de bestandsnaam, dus zonder het voorafgaande pad - in
    # tegenstelling tot self.bestand
    def bestandsnaam(self):
        return basename(self.bestand)


    # Levert een jaar op in string-formaat. Het hoeft dus geen int te zijn.
    # Dat is omdat sommige teksten geen precieze datum hebben, maar eerder
    # iets als 177x. Wel moet het gegeven jaartal 4 tekens lang zijn.
    # Dus 177x is oke.
    # Methode moet overschreven worden door subklassen.
    def jaar(self):
        print("Waarschuwing: ongeimplementeerde functie. Gebruik deze niet")
        return None


    # Methode die het genre van dit document geeft. Moet overschreven
    # worden door subklassen om een zinnige implementatie te geven. Het gegeven
    # genre is een van de hierboven gedefinieerde constanten (UNKNOWN,
    # SERMONS, LETTERS, ...). Met andere woorden, deze functie is de mapping
    # van het corpus zijn genres naar Bragi's begrip ervan.
    # Een subklasse moet deze methode overschrijven/herimplementeren
    def genre(self):
        print("Waarschuwing: ongeimplementeerde functie. Gebruik deze niet")
        return None


    # Deze methode geeft de naam van het genre zoals het corpus hem zelf
    # weergeeft. Dus 'x', 'w', 's' in Archer bv, of 'fiction', 'let priv', of
    # 'philosophy' in PPCEME.
    # Subklassen moeten deze methode herimplementeren.
    def genre_tekst(self):
        print("Waarschuwing: ongeimplementeerde functie. Gebruik deze niet")
        return None


    # Geeft de inhoud van het bestand zonder alle metadata en annotaties e.d.
    # Met andere woorden, het geeft de tekst zoals die in het oorspronkelijke,
    # destijds geschreven bestand stond.
    # Onderstaande functie werkt voor alle tekstbestanden in de zin dat het een
    # string met tekst geeft, maar als het bestand een xml-bestand is, dan geeft
    # het ook de xml mee. In zulke gevallen moeten subklassen dus deze methode
    # herimplementeren. Zie bv EmmaDocument.tekst() voor een voorbeeld.
    def tekst(self, versleuteling="utf-8"):
        # Hier volgt een naieve implementatie die ervan uitgaat dat er geen
        # annotaties en metadata in het bestand staan. Die aanname is
        # waarschijnlijk fout, dus subklassen moeten deze methode overschrijven
        if not self.luie_tekst:
            with open(self.bestand, "r", encoding=versleuteling) as bestand:
                tekst = bestand.read()
                self.luie_tekst = tekst
        return self.luie_tekst


    def tekst_xml_veilig(self):
        return self.tekst().replace("&", "&amp;")


    def relevant(self):
        return self.genre() != Document.NEGEER and Document.relevant_jaar(self.jaar())


    def woorden(self):
        # TODO make punctuation filtering optional
        tekst = self.tekst()
        regels = [regel.strip() for regel in tekst.split("\n")]
        woorden = [ woord.strip().lower().translate(
                        str.maketrans('', '', string.punctuation)
                    )
                    for regel in regels
                    for woord in regel.split(" ") ]
        return [woord for woord in woorden if woord != ""]


    @staticmethod
    def lees_in(pad, klasse, negeerlijst = [], zeef=lambda x: True, stil=False):
        if not stil:
            print("Inlezen van {}-map {}  ".format(klasse.naam, pad), end="…  ")

        if not isdir(pad): # ook als stil, want belangrijk
            print("mislukt, want bestaat niet of niet een map")
            print("Bragi geeft het op")
            exit(3)

        bestanden = [join(pad, doc) for doc in listdir(pad)
                     if (not doc in negeerlijst) and isfile(join(pad, doc))]
        docs = [klasse(bestand) for bestand in bestanden]
        docs = [doc for doc in docs if zeef(doc)]

        return docs


    @staticmethod
    def relevant_jaar(jaartal):
        if len(jaartal) != 4:
            return False

        if jaartal == "?177": # raar clmet jaartal
            return True

        try:
            eeuw = int(jaartal[:2])
            decennium = jaartal[2:3]

            if eeuw == 16:
                return True
            elif eeuw == 15:
                return decennium == "x" or int(decennium) >= 7
            elif eeuw == 17:
                return decennium =="x" or int(decennium) <= 7
            else:
                return False
        except:
            bericht = "Interpreteren van tekst-jaar als getals-jaar mislukte voor {}"
            print(bericht.format(jaartal))
            return False
