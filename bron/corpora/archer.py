from os import listdir
from os.path import isfile, join, isdir, basename, dirname, exists
from re import sub, split, search, compile, MULTILINE
from lxml import etree

from corpora.document import Document


# Klasse om Archer-documenten zoals in het oorspronkelijke, onbewerkte
# Archer-corpus voorkomen te begrijpen
class ArcherDocument(Document):
    naam = "Archer"
    # Verbanden tussen Archers genre-afkortingen en bragi's categorieen
    genres = { "d": Document.THEATER
             , "h": Document.SERMONS
             , "j": Document.TRAVEL
             , "l": Document.LEGAL
             , "m": Document.SCIENCE
             , "s": Document.SCIENCE
             , "x": Document.LETTERS
             , "f": Document.FICTION
             , "a": Document.ADVERTISING # advertising
             , "y": Document.DIARY # diaries
             , "n": Document.NEWS # news
             , "p": Document.NEGEER # early prose
             }

    luie_tekst_archer = None #: Optional[str]

    def bestandsnaam(self):
        return self.bestand[-20:]

    def jaar(self):
        return self.bestandsnaam()[:4]

    def genre(self):
        genre_afk = self.genre_tekst()
        return self.genres[genre_afk]

    def genre_tekst(self):
        return self.bestand[-7]

    def tekst(self):
        if not self.luie_tekst_archer:
            inhoud = super().tekst()

            # Eerste regel met "<?xml version="1.0" encoding="UTF-8"?>" verwijderen
            # Is een probleem als je vanaf een string inleest
            positie = inhoud.find("\n") + 1
            inhoud = inhoud[positie:]

            # XML blijkbaar niet volledig juist opgesteld. Verschillende xml:id
            # attributen doen de parsers weigeren de tekst verder te ontleden
            # Haal ze daarom weg
            # Ook eem html-commentaarstukken in 1666cav2_f2b deed lxml struikelen.
            # Deze is handmatig verwijderd
            inhoud = sub(' xml:id=".+"', "", inhoud)

            # Verkrijg tekst-inhoud van body
            try:
                root = etree.fromstring(inhoud)
                bodys = root.xpath(".//body")
                if len(bodys) == 1:
                    inhoud = ' '.join(bodys[0].itertext()).strip()
                    self.luie_tekst_archer = inhoud
                else:
                    str = "FOUT: bestand had niet precies 1 body: {0}"
                    print(str.format(self.bestand))
                    exit(1)
            except:
                print("FOUT: bestand ongeldig, {}".format(self.bestandsnaam()))
                raise
        return self.luie_tekst_archer

    @staticmethod
    def lees_in(pad, zeef=lambda x: True):
        docs = [ArcherDocument(bestand.bestand) for bestand
                in Document.lees_in(pad, ArcherDocument, zeef=zeef)]
        print("{} gevonden".format(len(docs)))
        return docs
