from os import listdir
from os.path import isfile, join, isdir, basename, dirname, exists
from re import sub, split, search, compile, MULTILINE
from lxml import etree

from corpora.document import Document


class PpcmbeDocument(Document):
    naam = "PPCMBE"
    genres = { "letters priv": Document.LETTERS
             , "letters non-priv": Document.LETTERS
             , "diary": Document.NEGEER
             , "educ treatise": Document.NEGEER
             , "proceedings trial": Document.LEGAL
             , "biography other": Document.BIOGR
             , "handbook other": Document.NEGEER
             , "biography auto": Document.BIOGR
             }
    penndoc = None
    tekstregels = None

    def __init__(self, bestand):
        self.bestand = bestand
        super().__init__(bestand)
        self.penndoc = PpcemeDocument(bestand)

    def jaar(self):
        naamdelen = self.bestandsnaam()[:-4].split('-')
        jaar_idx = len(naamdelen) - 1 # laatste, maar idx begint van 0, dus -1
        return naamdelen[jaar_idx]

    def genre(self):
        genre_tekst = self.genre_tekst()
        return self.genres[genre_tekst]

    def genre_tekst(self):
        if self.tekstregels == None:
            self.tekstregels = self.lees_infotekst()

        gevonden, genre = self.vind_voor_sleutel(self.tekstregels, "Genre")

        if not gevonden:
            bericht = "Genre kon niet gevonden worden in {}"
            bericht = bericht.format(self.bestandsnaam())
            raise SyntaxError(bericht)

        genre = search(r'>[a-zA-Z0-9 -_]+</', genre).group(0)[1:-2].strip()
        genre = genre.replace('_', ' ') # waar ppceme ' ' gebruikt, gebruikt ppcmbe '_'
        return genre

    def tekst(self):
        return self.penndoc.tekst()

    def interessant(self):
        jaar = int(self.jaar())
        return jaar >= 1570 and jaar <= 1780 and self.genre() != Document.NEGEER

    @staticmethod
    def lees_in(pad):
        docs = Document.lees_in(pad, PpcmbeDocument)
        print("{} gevonden".format(len(docs)))
        return [PpcmbeDocument(doc.bestand) for doc in docs]

    def lees_infotekst(self):
        info = self.vind_infobestand()

        inhoud = ""
        with open(info) as f:
            inhoud = f.read()
        regels = inhoud.splitlines()
        return regels

    def vind_voor_sleutel(self, inforegels, sleutel):
        volgende = False
        waarde = ""

        for regel in inforegels:
            if volgende:
                waarde = regel
                break
            if sleutel in regel:
                volgende = True

        if not volgende:
            print("Waarde-regel niet gevonden")

        return (volgende, waarde.lower())

    def vind_infobestand(self):
        info = dirname(self.bestand) + "/../../info/" + self.bestandsnaam()[:-4] + ".html"
        return info
