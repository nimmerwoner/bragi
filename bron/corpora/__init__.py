from typing import Callable, List

from corpora.document import Document
from corpora.archer import ArcherDocument
from corpora.bragi import BragiDocument
from corpora.clmet import ClmetDocument
from corpora.coerp import CoerpDocument
from corpora.emma import EmmaDocument
from corpora.helsinki import HelsinkiDocument
from corpora.ppceme import PpcemeDocument
from corpora.ppcmbe import PpcmbeDocument


def lees_archer(
        pad: str,
        zeef: Callable[[Document], bool] = lambda x: True
    ) -> List[Document]:
    return ArcherDocument.lees_in(pad, zeef)


def lees_bragi(
        pad: str,
        zeef: Callable[[Document], bool] = lambda x: True
    ) -> List[Document]:
    return BragiDocument.lees_in(pad, zeef)


def lees_clmet(
        pad: str,
        zeef: Callable[[Document], bool] = lambda x: True
    ) -> List[Document]:
    return ClmetDocument.lees_in(pad, zeef)


def lees_coerp(
        pad: str,
        zeef: Callable[[Document], bool] = lambda x: True
    ) -> List[Document]:
    return CoerpDocument.lees_in(pad, zeef)


def lees_emma(
        pad: str,
        zeef: Callable[[Document], bool] = lambda x: True
    ) -> List[Document]:
    return EmmaDocument.lees_in(pad, zeef)


def lees_helsinki(
        pad: str,
        zeef: Callable[[Document], bool] = lambda x: True
    ) -> List[Document]:
    return HelsinkiDocument.lees_in(pad, zeef)


def lees_ppceme(
        pad: str,
        zeef: Callable[[Document], bool] = lambda x: True
    ) -> List[Document]:
    return PpcemeDocument.lees_in(pad, zeef)


def lees_ppcmbe(
        pad: str,
        zeef: Callable[[Document], bool] = lambda x: True
    ) -> List[Document]:
    return PpcmbeDocument.lees_in(pad, zeef)
