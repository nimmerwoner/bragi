from os import listdir
from os.path import isfile, join, isdir, basename, dirname, exists
from re import sub, split, search, compile, MULTILINE
from lxml import etree

from corpora.document import Document

# Er wordt niet getraind op Clmet, omdat zelfs met alleen drama en let als
# trainingsdata, zakte Bragi's classificatie-accuratie van 95% tot wel 66%.
# Als ook nog narrative fiction aangezet werd, dook die accuratie zelfs tot
# maar 20%.
# De tweede situatie komt waarschijnlijk doordat narr fict een heel breed genre
# is, maar in het eerste geval is onduidelijk waarom en vind ik dat vreemd.
# Misschien dat het komt doordat clmet niet helemaal succesvol is opgeschoond
# en een paar vreemde tekenset-fouten heeft.
class ClmetDocument(Document):
    naam = "CLMET"
    genres = { "drama": Document.THEATER
             , "let": Document.LETTERS
             , "treatise": Document.NEGEER
             , "other": Document.NEGEER
             , "narrative fiction": Document.NEGEER
             , "narrative non-fiction": Document.NEGEER
             }


    def jaar(self):
        # Voorbeeldregel: <date of text>1768</date of text>
        jaar = None
        for regel in super().tekst(versleuteling="latin1").splitlines():
            if regel[:14] == "<date of text>":
                jaar = regel[14:18]
                break
        return jaar

    def genre(self):
        tekst = self.genre_tekst()
        return self.genres[tekst]

    def genre_tekst(self):
        # Voorbeeldregel: <genre>Drama</genre>
        genre_tekst = None
        for regel in super().tekst(versleuteling="latin1").splitlines():
            if regel[:7] == "<genre>":
                genre_tekst = regel.strip()[7:-8]
                break

        return genre_tekst.lower()

    def tekst(self):
        tekst = ""
        onthoud = False
        for regel in super().tekst(versleuteling="latin1").splitlines():
            # We willen alleen het stuk tussen de text-tags meenemen
            if regel.strip() == "<text>":
                onthoud = True
            elif regel.strip() == "</text>":
                onthoud = False

            if onthoud:
                tekst += regel
                tekst += "\n"

        return tekst


    def interessant(self):
        return self.genre() != Document.NEGEER

    @staticmethod
    def lees_in(pad):
        docs = [ClmetDocument(bestand.bestand) for bestand
                in Document.lees_in(pad, ClmetDocument)]
        print("{} gevonden".format(len(docs)))
        return docs
