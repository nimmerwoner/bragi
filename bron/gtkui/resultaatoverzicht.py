import os
from typing import Optional, Dict, Callable, List, Set
from pprint import pprint
from urllib.request import urlretrieve

from gi.repository import Gtk

from gtkui.controller import Controller
from ai import Bayes, SVM, FastText
from corpora import lees_emma, Document, lees_bragi, lees_archer, lees_ppceme
from gedeeld import bereken_precisies, bereken_recalls, bereken_overeenkomst


class Resultaatoverzicht(Controller):
    overzicht = None # Gtk.TreeView
    data = None #: Gtk.ListStore


    def __init__(self, definition: str = "middelen/resultaatoverzicht.ui"):
        super(Resultaatoverzicht, self).__init__(definition)
        self.overzicht, bouwer = self.load("boom_overzicht")
        self.data, _ = self.load("lijst_resultaten", bouwer)


    def toon_resultaten(self, voorspellingen: List[str], feiten: List[str] = []):
        # Bereken en toon hoeveel van elk genre voorspeld zijn
        aantallen = {} # Dict[str, int]
        for genre in voorspellingen:
            if genre not in aantallen:
                aantallen[genre] = 0
            else:
                aantallen[genre] += 1

        #accuratesses = bereken_accuratesses(voorspellingen, feiten)
        ps = bereken_precisies(voorspellingen, feiten)
        rs = bereken_recalls(voorspellingen, feiten)
        ptg = "{:.0%}"

        # TODO nul-resultaten toevoegen voor genres zonder voorspellingen
        rows = [(genre, str(aantal), ptg.format(ps[genre]), ptg.format(rs[genre]))
                for genre, aantal in aantallen.items()]
        rows.sort(key=lambda row: row[0])
        [self.data.append(row) for row in rows]
