import os
from typing import Tuple, Any

from gi.repository import Gtk

class Controller:
    def __init__(self, definition: str):
        file_directory = os.path.dirname(os.path.abspath(__file__))
        self.definition = os.path.join(file_directory, definition)

    def load(self, object_id_str: str, builder: Gtk.Builder = None
    ) -> Tuple[Any, Gtk.Builder]:
        if not builder:
            builder = Gtk.Builder.new_from_file(self.definition)
            builder.connect_signals(self)
        return builder.get_object(object_id_str), builder
