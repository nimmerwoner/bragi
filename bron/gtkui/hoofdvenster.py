import os
import threading
from typing import Optional, Dict, Callable, List, Set
from pprint import pprint
from urllib.request import urlretrieve

from pandas import DataFrame
from gi.repository import Gtk, GLib

from gtkui.controller import Controller
from gtkui.resultaatoverzicht import Resultaatoverzicht
from ai import Bayes, SVM, FastText
from corpora import lees_emma, Document, lees_bragi, lees_archer, lees_ppceme


class Hoofdvenster(Controller):
    # Widgets
    venster = None #: Gtk.ApplicationWindow
    modellenmenuknop = None #: Gtk.Button
    modellenmenu = None #: Gtk.PopoverMenu
    analyseerknop = None #: Gtk.Button
    exporteerknop = None #: Gtk.Button
    corpuspadkiezerknop = None #: Gtk.FileChooserButton
    corpussoortknop = None #: Gtk.ComboBox
    voorspellingenlijst = None #: Gtk.ListStore
    genretallenlijst = None #: Gtk.ListStore
    weergaves = None #: Gtk.Stack
    resultaten = None # Gtk.Notebook
    analysevoortgangs = None # Gtk.ProgressBar

    # Data
    voorspellingen_set = {} #: Dict[str, List[str]]
    geanalyseerde_documenten = [] #: List[Document]

    ontleders = { # Dict[str, Callable[[str], List[Document]]]
        "Emma": lees_emma,
        "Bragi": lees_bragi,
        "Archer": lees_archer,
        "PPCEME": lees_ppceme
    }


    def __init__(self, definition: str = "middelen/hoofdvenster.ui"):
        super(Hoofdvenster, self).__init__(definition)
        self.venster, bouwer = self.load("hoofdvenster")
        self.laad_modellenmenu(bouwer)
        self.analyseerknop, _ = self.load("knop_analyseer", bouwer)
        self.exporteerknop, _ = self.load("knop_exporteer", bouwer)
        self.corpuspadkiezerknop, _ = self.load("knop_corpuspadkiezer", bouwer)
        self.corpussoortknop, _ = self.load("knop_corpussoort", bouwer)
        self.modellenlijst, _ = self.load("lijst_modellen", bouwer)
        self.overeenkomstlijst, _ = self.load("lijst_overeenkomstpercentages",
                                              bouwer)
        self.genretallenlijst, _ = self.load("lijst_genretallen", bouwer)
        self.weergaves, _ = self.load("stapel_weergaves", bouwer)
        self.resultaten, _ = self.load("ordner_resultaten", bouwer)
        self.analysevoortgang, _ = self.load("voortgang_analyse", bouwer)


    def activeer_analyseerknop(self, _: Gtk.FileChooser):
        # FIXME schakelen naar een niet-map mag van de control, maar triggert
        # deze callback niet -> analyseerknop staat foutief op gevoelig
        uri = self.neem_gekozen_uri()
        self.analyseerknop.set_sensitive(os.path.isdir(uri))


    def analyseer(self, _: Gtk.Button):
        # TODO Betere feedback dat/waarmee hij bezig is

        # Resultaatvariabelen herinitialiseren en tabbladen met resultaten
        # verwijderen
        self.weergaves.set_visible_child_name("pagina_afwachting")
        self.geanalyseerde_documenten = []
        self.voorspellingen_sets = {}
        [self.resultaten.remove_page(idx) for idx
         in range(self.resultaten.get_n_pages())[::-1]]
        self.analyseerknop.set_sensitive(False)
        self.corpuspadkiezerknop.set_sensitive(False)
        self.corpussoortknop.set_sensitive(False)
        self.exporteerknop.set_sensitive(False)
        self.analysevoortgang.set_visible(True)
        self.analysevoortgang.pulse()

        # Add result pages, update references to data and enable export button
        def verwerk(voorspellingen_sets, feiten):
            self.analysevoortgang.pulse()
            self.toon_voorspellingen(voorspellingen_sets, feiten)
            self.voorspellingen_sets = voorspellingen_sets
            self.analyseerknop.set_sensitive(True)
            self.corpuspadkiezerknop.set_sensitive(True)
            self.corpussoortknop.set_sensitive(True)
            self.exporteerknop.set_sensitive(True)
            self.analysevoortgang.pulse()
            self.analysevoortgang.set_visible(False)

        def voortgang(tekst: str):
            self.analysevoortgang.set_text(tekst)
            self.analysevoortgang.pulse()

        def doe():
            # Laad de benodigde algoritmes: download de modellen als nodig
            GLib.idle_add(voortgang, "Algoritmen laden")
            algoritmen = self.laad_algoritmen()

            # Lees in
            uri = self.neem_gekozen_uri() # haal uri uit widget
            GLib.idle_add(voortgang, "Inlezen van " + uri)
            if not os.path.isdir(uri):
                return
            ontleder = self.bepaal_corpusontleder()
            self.documenten = ontleder(uri)

            # Analyseer
            voorspellingen_sets = {}
            for alg in algoritmen:
                naam = alg.__class__.__name__
                GLib.idle_add(voortgang, "Analyseren met " + naam)
                voorspellingen_sets[naam] = alg.voorspel(self.documenten)

            # Toon resultaten
            GLib.idle_add(voortgang, "Resultaten tonen")
            analysesetnaam = os.path.basename(uri)
            feiten = [doc.genre() for doc in self.documenten]
            voorspellingen_sets[analysesetnaam] = feiten

            # Eindresultaten opmaken
            GLib.idle_add(verwerk, voorspellingen_sets, feiten)

        analyse = threading.Thread(target=doe)
        analyse.daemon = True
        analyse.start()


    def exporteer(self, _: Gtk.Button):
        df = DataFrame()
        df['bestand'] = [doc.bestandsnaam() for doc
                         in self.geanalyseerde_documenten]
        for setnaam, voorspellingen in self.voorspellingen_sets.items():
            df[setnaam] = voorspellingen
        df.to_csv("/tmp/bragi.csv")
        del df


    def toggle_modellenmenu(self, modellenmenuknop: Gtk.ToggleButton):
        if modellenmenuknop.get_active():
            self.modellenmenu.popup()
        else:
            self.modellenmenu.popdown()


    def sluit_modellenmenu(self, _: Gtk.Popover):
        self.modellenmenuknop.set_active(False)


    def laad_modellenmenu(self, bouwer: Gtk.Builder):
        self.modellenmenuknop, _ = self.load("knop_modellen", bouwer)
        self.modellenmenu, _ = self.load("popover_modellen", bouwer)


    def neem_gekozen_uri(self) -> str:
        """ Vraagt de geselecteerde uri op en verwijdert de protocol-prefix
            (file:). Geeft de gehele uri als er geen protocol in staat.
        """
        uri = self.corpuspadkiezerknop.get_uri().split(":")
        return uri[1] if len(uri) > 1 else uri[0]


    def bepaal_corpusontleder(self) -> Callable[[str], List[Document]]:
        actief_ref = self.corpussoortknop.get_active_iter()
        actief_naam = self.corpussoortknop.get_model()[actief_ref][0]
        return self.ontleders[actief_naam]


    def toon_voorspellingen(self, voorspellingensets, feiten):
        for setnaam, voorspellingen in voorspellingensets.items():
            overzicht = Resultaatoverzicht()
            self.resultaten.append_page(overzicht.overzicht, Gtk.Label(setnaam))
            overzicht.toon_resultaten(voorspellingen, feiten)

        self.weergaves.set_visible_child_name("pagina_resultaten")


    def laad_algoritmen(self):
        algoritmen = [
            ("Bayes-latest.pkl", lambda m: Bayes(getraind_model=m)),
            ("SVM-latest.pkl", lambda m: SVM(getraind_model=m)),
            ("FastText-latest.ftz", lambda m: FastText(getraind_model=m)),
        ]

        return [self.laad_algoritme(naam, alg) for naam, alg in algoritmen]


    def laad_algoritme(
            self,
            algoritmenaam: str,
            algoritme: Callable[[str], object]
    ) -> List[object]:
        folder = os.path.dirname(os.path.abspath(__file__))
        folder = os.path.join(folder, "../middelen/")
        bestand = os.path.join(folder, algoritmenaam)
        if not os.path.isfile(bestand):
            self.download_model(algoritmenaam, folder)
        return algoritme(bestand)


    def download_model(
            self,
            naam: str,
            opslagfolder: str,
            bron: str = "http://nimmerfort.eu/bragi/middelen/"
    ):
        os.makedirs(opslagfolder, exist_ok=True)
        download_uri = bron + naam
        opslag_uri = os.path.join(opslagfolder, naam)
        urlretrieve(download_uri, opslag_uri)
