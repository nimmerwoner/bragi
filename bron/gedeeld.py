from typing import List, Dict, Tuple, Callable, Optional, Iterator
from nptyping import Array

from sklearn.metrics import accuracy_score, precision_score, recall_score


# FIXME volgorde van voorspellingen / feiten omkeren
def bereken_overeenkomst(
    voorspellingen: List[str],
    feiten: List[str],
    onbekenden: List[str] = []
) -> float:
    samen = list(filter_negeerbaren(voorspellingen, feiten, onbekenden))
    aantal = len(samen)
    gelijk = 0
    for c1, c2 in samen:
        if c1 == c2:
            gelijk += 1
    return gelijk / aantal


def bereken_precisies(
    voorspellingen: List[str],
    feiten: List[str],
    onbekenden: List[str] = []
) -> Dict[str, float]:
    samen = filter_negeerbaren(voorspellingen, feiten, onbekenden)
    return bereken_scores(voorspellingen, feiten, precision_score)


def bereken_recalls(
    voorspellingen: List[str],
    feiten: List[str],
    onbekenden: List[str] = []
) -> Dict[str, float]:
    samen = filter_negeerbaren(voorspellingen, feiten, onbekenden)
    return bereken_scores(voorspellingen, feiten, recall_score)


def bereken_accuratesses(
    voorspellingen: List[str],
    feiten: List[str],
    onbekenden: List[str] = []
) -> Dict[str, float]:
    samen = filter_negeerbaren(voorspellingen, feiten, onbekenden)
    return bereken_scores(voorspellingen, feiten, accuracy_score)


def bereken_scores(
    voorspellingen: List[str],
    feiten: List[str],
    scoorder: Callable[[List[int], List[int], Optional[str]], Array[int]]
) -> Dict[str, float]:
    namen = list(set(voorspellingen).union(set(feiten)))
    nummers = {naam: idx for idx, naam in enumerate(namen)}

    v_nummers = [nummers[v] for v in voorspellingen]
    f_nummers = [nummers[f] for f in feiten]
    scores = scoorder(f_nummers, v_nummers, average=None)

    return {naam: scores[idx] for idx, naam in enumerate(namen)}


def filter_negeerbaren(
    set1: List[str],
    set2: List[str],
    negeerbaren: List[str]
) -> Iterator[Tuple[str, str]]:
    assert len(set1) == len(set2), \
           "Onverwacht ongelijk aantal classificaties in twee sets"

    verwijder_indices = []
    for idx, (c1, c2) in enumerate(zip(set1, set2)):
        if c1 in negeerbaren or c2 in negeerbaren:
            verwijder_indices.append(idx)

    # TODO efficienter zodat in 1 keer over lijst
    set1 = set1.copy()
    set2 = set2.copy()
    for idx in verwijder_indices[::-1]:
        set1.pop(idx)
        set2.pop(idx)

    assert len(set1) == len(set2), \
           "Onverwacht ongelijk aantal classificaties in twee sets"
    return zip(set1, set2)
