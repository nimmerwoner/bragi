TE DOEN
=======

## GTK UI
1. ~GTK UI die toestaat getrainde modellen toe te passen op een ondersteunt
   corpus~
2. Meer gebruikersfeedback geven
3. Controleren waarom downloaden en toepassen modellen niet werkt met flatpak
4. Bragi-icoon in programma verwerken
5. Info-scherm verwerken
6. Kleine handleiding schrijven?

## Trainen
1. ~Getrainde modellen opslaan en inladen~
2. Trainen op alle genres
3. Een nieuw algoritme gebruikmakend van grote taalmodellen (GPT2) werkt beter

## Testen
1. ~Accuratesse, precisie en recall berekenen zoals gedefinieerd in https://programminghistorian.org/en/lessons/naive-bayesian#understanding-naive-bayesian-classification-using-a-generative-story~

## Corpora
0. Platte tekst, woord per regel kunnen lezen
1. PPCEME normaliseren
2. COERP toevoegen
3. Corpora gestructureerder inlezen:
    a. Inlezen met NLTK's tokenizer
    b. Lemmatiseren of stemmen (https://www.datacamp.com/community/tutorials/stemming-lemmatization-python)
    c. Opschonen (https://programminghistorian.org/en/lessons/normalizing-data)
4. Documenten van PPCEME waarop getraind wordt onderzoeken om te zien of ze
   netjes zijn

## Flatpak
1. ~Desktop-icoon toevoegen~
2. ~Repo deployen~
3. App Metadata schrijven
4. Flatpak Manifest systeem onafhankelijk maken

## Overige verbeteringen
1. Uitzoeken waarom en oplossen dat Bragi 60 MB groot is om te installeren
   (200 MB uitgepakt, ligt aan sklearn + scipy + numpy).
2. Uitzoeken of LXML vervangen kan worden met iets anders
